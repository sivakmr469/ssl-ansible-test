#!/bin/bash

filename="servers.txt"

while read -r line; do
    i="$line"
    echo ${i}
    cp server.key "${i}.key" && cp server.crt "${i}.crt"

        if [ $? != 0 ]; then
            echo "some error, please check"
            exit 1
        fi
done < "$filename"